let tabs= document.querySelector(".service-tabs")
let contentUl =document.body.querySelector(".service-tabs-content")
let contentLi =Array.from(contentUl.children)
contentLi.forEach(el=>{el.style.display="none"})
contentLi[0].style.display="flex"
Array.from(tabs.children)[0].classList.add("active")
function activ (e){
    
let target = e.target
Array.from(tabs.children).forEach(el=>{if (el==target){
   el.classList.add("active")} else el.classList.remove("active")})
   
contentLi.map(el=> { if(el.dataset.name== target.dataset.content) el.style.display="flex"
  else {el.style.display="none" }})

}
tabs.addEventListener("click",activ)
let workButton=document.querySelector(".work-tab-button")

const imgWork =[
    "img/Web Design/01.jpg",
    "img/Web Design/02.jpg",
    "img/Web Design/03.jpg",
    "img/Graphic Design/01.jpg",
    "img/Graphic Design/02.jpg",
    "img/Graphic Design/03.jpg",
    "img/Landing Page/01.jpg",
    "img/Landing Page/02.jpg",
    "img/Landing Page/03.jpg",
    "img/Wordpress/01.jpg",
    "img/Wordpress/02.jpg",
    "img/Wordpress/03.jpg",
    "img/Wordpress/04.jpg",
    "img/Wordpress/05.jpg",
    "img/Wordpress/06.jpg",
    "img/Landing Page/04.jpg",
    "img/Landing Page/05.jpg",
    "img/Landing Page/06.jpg",
    "img/Web Design/04.jpg",
    "img/Web Design/05.jpg",
    "img/Web Design/06.jpg",
    "img/Graphic Design/04.jpg",
    "img/Graphic Design/05.jpg",
    "img/Graphic Design/06.jpg",
]
function r(){
imgWork.forEach((el, i) => { if (i<=11){ 
    const img = document.createElement('img');
    img.className = 'work-tabs-content-img';
    img.src = el;
    const div = document.createElement('div');
    div.className = 'work-tabs-content-container';
    div.setAttribute("data-name", `${el.slice(4,-7)}`);
    div.setAttribute("data-all", "All");
    div.append(img);

    document.querySelector('.work-tabs-content').append(div);}
  
})}
r()

let divWorkContainer=document.querySelectorAll(".work-tabs-content-container")
function hoverWorkContainer () {
    Array.from(divWorkContainer).forEach(el=>{el.insertAdjacentHTML("beforeend",`<div class="work-tabs-content-hover">
    <img class="work-tabs-content-hover-img" src="./img/line-hover.png" alt="">
    <div class="work-tabs-content-hover-button">
        <a href="#"><img src="./img/icon1-hover.png" alt=""></a>
        <a href="#"><img src="./img/icon2-hover.png" alt=""></a>
    </div>
        <p class="work-tabs-content-hover-text-design">CREATIVE DESIGN</p>
        <p class="work-tabs-content-hover-text-class">${el.dataset.name}</p>
    </div>`)})
}
hoverWorkContainer()

let workTabs= document.querySelector(".work-tabs")
function activ2 (e){
    Array.from(divWorkContainer).forEach(el=> {el.style.display="none"})
let target = e.target
if (target.dataset.content!="All"){
    workButton.style.display="none"  
} else {
    workButton.style.display="flex" 
}
Array.from(workTabs.children).forEach(el=>{if (el==target){
   el.classList.add("active")} else el.classList.remove("active")})
   
Array.from(divWorkContainer).map(el=> { if(el.dataset.name== target.dataset.content||el.dataset.all== target.dataset.content) el.style.display="grid"
  else {el.style.display="none" }})

}
workTabs.addEventListener("click",activ2)
let balanim = document.querySelector(".ball")
balanim.style.display="none"
function loadImg (){
    workButton.style.display="none"
    balanim.style.display="block"
    setTimeout(function(){
        balanim.style.display="none"
    imgWork.forEach((el, i) => { if (i>11 & i<=24){ 
        const img = document.createElement('img');
        img.className = 'work-tabs-content-img';
        img.src = el;
        const div = document.createElement('div');
        div.className = 'work-tabs-content-container';
        div.setAttribute("data-name", `${el.slice(4,-7)}`);
        div.setAttribute("data-all", "All");
        div.append(img);
    
        document.querySelector('.work-tabs-content').append(div);}
    
    })
    divWorkContainer=document.querySelectorAll(".work-tabs-content-container")
    hoverWorkContainer()
},2000)}

workButton.addEventListener("click",loadImg)


let ImageNews=[
    "img/work/1.png",
    "img/work/2.png",
    "img/work/3.png",
    "img/work/4.png",
    "img/work/5.png",
    "img/work/6.png",
    "img/work/7.png",
    "img/work/7.png",
]
ImageNews.forEach(el => { 
    const img = document.createElement('img');
    img.className = 'news-img';
    img.src = el;
    const a = document.createElement('a');
    a.className = 'news-link';
    a.append(img);
    const pHeader=document.createElement('p')
    pHeader.className="news-header"
    pHeader.innerText="Amazing Blog Post"
    a.append(pHeader);
    a.setAttribute("href","#")
    const pText=document.createElement('p')
    pText.className ="news-text"
    const pTextComent=document.createElement('p')
    pText.innerText="By admin"
    pTextComent.innerText= "2 comment"
    pTextComent.className ="news-text news-text-coment"
    a.append(pText);
    a.append(pTextComent);
    document.querySelector('.news-wrapper').append(a);
  
})
const people=[ 
    {
        name:"HASAN ALLI",
        profession:"UX Designer",
        coment:" Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
        img:"img/people/hasan.png"
    },
    {
        name:"ADAM ALLI",
        profession:"UX Designer",
        coment:" Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
        img:"img/people/adam.png"
    },
    {
        name:"NICK ALLI",
        profession:"UX Designer",
        coment:" Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
        img:"img/people/nick.png"
    },
    {
        name:"SARA ALLI",
        profession:"UX Designer",
        coment:" Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
        img:"img/people/sara.png"
    }

]

Array.from(people).forEach(el=>{
    const li=document.createElement("li")
    li.className="people-say-tabs-content-li"
    const pcoment=document.createElement("p")
    pcoment.classList= "people-say-tabs-content-coment"
    const pprof=document.createElement("p")
    pprof.classList="people-say-tabs-content-profession"
    const pname=document.createElement("p")
    pname.classList="people-say-tabs-content-name"
    imgPeople=document.createElement("img")
    imgPeople.classList="people-say-tabs-content-img"
    li.append(pcoment)
    
    li.append(pname)
    li.append(pprof)
    li.append(imgPeople)
    pcoment.innerText=el.coment
    pprof.innerText=el.profession
    pname.innerText=el.name
    imgPeople.src=el.img
document.querySelector(".people-say-tabs-content").append(li)
})


   { $(".people-say-tabs-content").slick({
   
    arrows:false,
    slidesToShow: 1, 
    slidesToScroll: 1,
    arrows:true,
    dots:true,
    speed:1000

   }
   )}

$(document).ready(function(){
    
    let container =$(".gallery-wrapper")
    container.masonry({
            itemSelector:".gallery-block",
            // columnWidth:".size3",
            percentPosition:true,
            gutter:20
    })

})




